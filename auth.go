package aityk

import (
    "crypto/hmac"
    "crypto/md5"
    "crypto/sha1"
    "encoding/base64"
    "fmt"
    "io/ioutil"
    "net/http"
    "net/url"
    "strings"
    "time"
)

type TykAuth struct {
    Key         string
    Secret      string
    Header      http.Header
}

func (auth *TykAuth) Use(r *http.Request) {
    date := time.Now().Format(time.RFC1123)
    body, _ := r.GetBody()
    buf, _ := ioutil.ReadAll(body)
    digest := auth.makeDigest(buf)

    request := fmt.Sprintf("(request-target): %s %s\n", strings.ToLower(r.Method), r.URL.Path)
    request += fmt.Sprintf("date: %s\n", date)
    request += fmt.Sprintf("digest: %s", digest)
    headers := "date digest"
    for name, _ := range auth.Header {
        h := auth.Header.Get(name)
        if h != "" {
            request += fmt.Sprintf("\n%s: %s",strings.ToLower(name), h)
            headers += " " + strings.ToLower(name)
            r.Header.Set(name, h)
        }
    }
    sign := auth.makeSignature([]byte(request))
    r.Header.Set("Authorization", fmt.Sprintf(`Signature keyId="%s",algorithm="hmac-sha1",headers="(request-target) %s",signature="%s"`, auth.Key, headers, sign))
    r.Header.Set("Date", date)
    r.Header.Set("Digest", digest)
}

func (auth *TykAuth) makeDigest(body []byte) string {
    h := md5.New()
    h.Write(body)
    return fmt.Sprintf("md5=%s", base64.StdEncoding.EncodeToString(h.Sum(nil)))
}

func (auth *TykAuth) makeSignature(message []byte) string {
    h := hmac.New(sha1.New, []byte(auth.Secret))
    h.Write(message)
    return url.QueryEscape(base64.StdEncoding.EncodeToString(h.Sum(nil)))
}